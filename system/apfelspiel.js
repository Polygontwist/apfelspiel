/*
	www.a-d-k.de	
	TODO:
		-nomanipulateJS
		->Ton kann nach matsch weiterlaufen!!
	http://docs.phonegap.com/en/3.5.0/guide_platforms_ios_config.md.html#iOS%20Configuration
*/

//---Game---
function apfelspiel(zielID){  
	//const ->IE Win7mobile kennt kein const!
	var mode_pause=1;
	var mode_play=2;
	var mode_bestenlisteIN=3;
	var mode_bestenlisteList=4;
	var mode_help=5;
	
	var sound_Matsch=1;
	var sound_Bestenliste=2;
	
	//---Helper---
	var cE=function(z,e,id,cn){
			l=document.createElement(e);
			if(id!="" && id!=undefined)l.id=id;
			if(cn!="" && cn!=undefined)l.className=cn;
			if(z)z.appendChild(l);
			return l;
		}
	var gE=function(id){if(id=="")return undefined; else return document.getElementById(id);}
	var setClass=function(ob,s){if(ob!=undefined)ob.className=s;}
	
	var settransform=function(e,s){
		e.style.webkitTransform =s;
		e.style.MozTransform =s;
		e.style.msTransform =s;
		e.style.OTransform =s;
		e.style.transform=s;
	}
	var addEvent=function(element, event, fn) {
			if (element.addEventListener)
				element.addEventListener(event, fn, false);
			else if (element.attachEvent)
				element.attachEvent('on' + event, fn);
		}
	
	var iniButton=function(a,f_onclick){
		if(a!=undefined){
			
			if(is_touch_device()){
				a.addEventListener("touchstart", function(event){f_onclick();event.preventDefault();}, !1);
			}
			else{
				a.onclick=f_onclick;
				a.href="#";
			}
			
		}
		return a;
	}
	
	var is_touch_device=function (){
		return (navigator.userAgent.match(/iPad/i) || 
			navigator.userAgent.match(/iPhone/i) || 
			navigator.userAgent.match(/Android/i) || 
			navigator.userAgent.match(/webOS/i));
		//return !!('ontouchstart' in window)  ;
		}
	
	var ziel=gE(zielID);
	
	var audio;
	
	var data={
		screen:{width:238,height:178},
		fps:30,
		positionenXm:[31,77,123,169,215],
		positionenYm:[9,64,102,140],
		figur:{pos:1,stat:0,lastR:1},
		
		punkte:0,
		leben:0,
		ballstep:1,
		aktuellesJahr:2014,
		
		audioready:false,
		audioisplay:0,
		
		pausegrafik:["P","A","U","S","E"],
		pausegrafikdiv:[],
		
		gewinnerlisteURL:"userliste.php",
		gewinnerlisteAddURL:"adduser.php",
		gewinnerliste:[ ]
	};
	
	var strings={
		punkte:"Punkte:",
		
		b_start:"Start",
		b_pause:"Pause",
		
		help1:"Das Spiel ist zuende!",
		help2:"Du kommst auf die Bestenliste:",
		help3:"Name:",
		help4:"speichern",
		
		best1:"Die Besten ",
		
		stat_speichern:"scheichern...",
		
		info:"<h1>Infos zum Apfelspiel</h1>\
		<p>Ziel des Spieles ist es die herunterfallenden Äpfel einzusammeln. Nach jeweils sieben Äpfel erhöht sich die Geschwindigkeit! Dafür gibt es aber auch mehr Punkte pro gefangenen Apfel.</p>\
		<h1>Bedienung</h1>\
		<p>Du kannst das Männchen über die Schaltflächen oder den Cursortasten Deiner Tastatur steuern. Mit SPACE wechselst Du den Pausen/Spiel-Modus.</p>\
		<p>Nach einem erfolgreichen Spiel kannst Du Dich in die Bestenliste eintragen.</p>\
		<p>Viel Spaß!</p>\
		<h1>Impressum</h1>\
		<p>Grafik, Programm:<br />Andreas Kosmehl</p>\
		<p>eMail: forum@a-d-k.de</p>\
		"
		
	}
	
	
	//Div-Objekte
	var basis,spielscreen,lebensliste,spielfigur,spielball,punkte,kaefer,
		bestenscreen,inputname,inputbutt,leben=[],buL,buR,bustartpause,
		spielmode=mode_pause;
	
	var ua = navigator.userAgent.toLowerCase(); 
	var is_safari = false;
	if (ua.indexOf('safari') != -1) { 
		if (ua.indexOf('chrome') < 0){
		 is_safari=true;
		}
	  }
	
	var gStr=function(id){
		var re=strings[id];
		if(re==undefined)re=id;
		return re;
	}
	
	var logDIV;
	var log=function(s){logDIV.innerHTML=s;}
	
	var ini=function(){		
		basis=cE(ziel,"div","","apfelspiel");
		
		document.onkeydown=keyDown;
		//basis.addEventListener("keydown",keyDown);
		
		var backscreen=cE(basis,"div","","apfel_backscreen");
		spielscreen=cE(basis,"div","","apfel_screen");
		
		var bs,t;
		lebensliste=cE(spielscreen,"div","","apfel_lebenliste");
		for(t=0;t<6;t++){
			bs=cE(lebensliste,"div","","apfel_noleben");
			bs.style.top=(t*20)+"px";
			leben.push(bs);
		}
		
		for(t=0;t<data.pausegrafik.length;t++){
			bs=cE(spielscreen,"div","","apfel_pause");
			bs.counter=0;
			bs.counterMax=(50+Math.random()*50)|0; //50...100
			bs.pos=(Math.random()*(data.positionenYm.length-2))|0;
			bs.innerHTML=data.pausegrafik[t];
			bs.style.left=((data.positionenXm[t]-bs.offsetWidth*0.5)|0)+'px';
			bs.style.top=data.positionenYm[bs.pos]+'px';
			data.pausegrafikdiv.push(bs);		
		}
		
		spielball=cE(spielscreen,"div","","apfel_apfel");
		spielball.style.display="none";
		setspielball();
		
		
		spielfigur=cE(spielscreen,"div","","apfel_spielfigur");
		
		punkte=cE(basis,"div","","apfel_punkte");
		showPunkte();
		
		logDIV=cE(basis,"div","","apfel_punkte");
		logDIV.style.left=0;
		logDIV.style.top=0;
		
		
		buL=cE(basis,"a","","apfel_butt apfel_buttLeft");
		buL.onkeydown=keyDown;
		iniButton(buL,click_L);
		buR=cE(basis,"a","","apfel_butt apfel_buttRight");
		buR.onkeydown=keyDown;
		iniButton(buR,click_R);
		bustartpause=cE(basis,"a","","apfel_butt apfel_buttStartPause");
		bustartpause.onkeydown=keyDown;
		iniButton(bustartpause,click_startstop);
		
		kaefer=cE(basis,"div","","apfel_kaefer");
		
		
		bs=cE(basis,"p","","apfel_helpp");
		var butthelp=cE(bs,"a","","apfel_butt");
		butthelp.innerHTML="?";
		iniButton(butthelp,click_help)
		
		bestenscreen=cE(spielscreen,"div","","apfel_bestenliste");
		setGameMode(mode_pause);
		
		
		
		
		audio=cE(spielscreen,"audio","","");		
		//mp3 oder ogg
		if(audio.canPlayType('audio/mpeg;').replace(/^no$/,'')!="")
			audio.setAttribute('src', 'media/sound.mp3');
		else
		if(audio.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,'')!="")
			audio.setAttribute('src', 'media/sound.ogg');			
		
		audio.load();
		audio.addEventListener("loadeddata", function() {audio.pause(); }, true);
		audio.addEventListener("error", function() {console.log("Audio ERROR")}, true);
		audio.addEventListener('canplay', function() {audio.pause();data.audioready=true;}, false);
		
		zoom();
		
		pausentimerData.counterstart=new Date();
		start();
		
		loadBestenliste();
		
	}
	
	function zoom(){
		var mulX=ziel.offsetWidth/basis.offsetWidth;
		var scb=basis.offsetWidth;
		var mulY=ziel.offsetHeight/basis.offsetHeight;
		if(mulX>mulY){
			mulX=mulY;
			scb=basis.offsetHeight;
			}
		
		var tr=scb/2*mulX - scb/2;
		var s="translate("+(tr|0)+"px,"+(tr|0)+"px) "+"scale("+mulX+") ";
		
		settransform(basis,s);
	}
	
	
	var click_help=function(e){
		if(spielmode!=mode_help)
			setGameMode(mode_help);
			else
			setGameMode(mode_pause);
	}
	
	var click_L=function(e){if(spielmode==mode_play)moveFigur(-1);}
	var click_R=function(e){if(spielmode==mode_play)moveFigur(1);}
	var click_startstop=function(e){
		if(!data.audioready)audio.load();
		switchGameMode();
	}

	
	//--requestAnimationFrame--
	var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
	var animationStartTime,requestId;
	function animate(time) {//60fps
	  timer();
	  requestId = requestAnimationFrame(animate);
	}
	function start() {
	  if(requestAnimationFrame){
			if(window.performance)
				animationStartTime = window.performance.now();
			data.fps=60;
			requestId = requestAnimationFrame(animate);
		}
		else{
			window.setInterval(timer, 1000/data.fps);//30fps
			}
	}
	function stop() {
	  if (requestId)
		if(window.cancelAnimationFrame)window.cancelAnimationFrame(requestId);
	  requestId = 0;
	}
	//--requestAnimationFrame--
	
	
	var timer=function(){
		var now=new Date();
		switch(spielmode){
			case mode_pause:
			case mode_bestenlisteList:
				pausenTimer(now);
				break;
			case mode_play:
				spielTimer(now);
				break;
		}
		
		//stopp Audios
		if(data.audioready){
			if(data.audioisplay>0){
				if(data.audioisplay==sound_Matsch){
					if(audio.currentTime>1.6)
						{audio.pause();data.audioisplay=0;}
						else audio.play();
				}
				if(data.audioisplay==sound_Bestenliste){
					if(audio.currentTime>4.6)
						{audio.pause();data.audioisplay=0;}
						else audio.play();
				}
			}
		}
	}
	
	var playSounds=function(i){
		if(data.audioready)
		{
			switch(i){
			 case sound_Matsch:
				audio.currentTime=1;
				audio.play();
				break;
			 case sound_Bestenliste:
				audio.currentTime=2;
				audio.play();
				break;
			}			
			data.audioisplay=i;
		}
	}
	
	var pausentimerData={counterstart:0};
	var pausenTimer=function(now){	
		var counterdiff=now-pausentimerData.counterstart;		
		if( counterdiff>500){
			pausentimerData.counterstart=now;
			var r=((Math.random()*10-5))|0;
			if(r<0) r=-1; else r=1;
			data.figur.lastR=r;
			moveFigur(r)
		}
		pausenani();
	}
	var pausenani=function(){//30fps
		var bs,t;
		for(t=0;t<data.pausegrafik.length;t++){
			bs=data.pausegrafikdiv[t];
			bs.counter++;
			if(bs.counter>bs.counterMax){
				bs.counter=0;
				bs.pos++;
				if(bs.pos>=data.positionenYm.length)bs.pos=0;
				bs.style.top=data.positionenYm[bs.pos]+'px';
			}
		}		
	}
	
	var setspielball=function(){
		var appelDiv=spielball;
		appelDiv.posY=0;
		var xpos=(Math.random()*(data.positionenXm.length-1))|0;
		var b=appelDiv.offsetWidth;
		if(b==0)b=38;
		var pos=(data.positionenXm[xpos]-b*0.5)|0;
		appelDiv.style.top='0px';
		appelDiv.style.left=pos+"px";
	}
	
	var ZahlToStr=function(n,z){
		var re=n+"";
		while(re.length<z)re='0'+re;
		return re;
	}
	
	var showBestenliste=function(){
		var t,ul,li,ob,s1,s2,s3;
		bestenscreen.innerHTML="";
		
		if(spielmode==mode_bestenlisteIN){
			s1=cE(bestenscreen,"h1","","");
			s1.innerHTML=gStr("help1");
			
			s1=cE(bestenscreen,"p","","");
			s1.innerHTML=gStr("help2");
			
			s1=cE(bestenscreen,"div","","");
			
			s2=cE(s1,"span","","");
			s2.innerHTML=gStr("help3");
			//input
			inputname=cE(s1,"input","","");
			inputname.setAttribute("size","11");
			inputname.setAttribute("maxlength","11");
			inputname.onkeypress=function(e){ if(e.keyCode==13)inputbutt.onclick();}
			
			//speichernbutton
			s1=cE(bestenscreen,"h1","","");
			inputbutt=cE(s1,"a","","apfel_butt apfel_buttsave");
			inputbutt.innerHTML=gStr("help4");			
			iniButton(inputbutt,click_save);
			
			inputname.focus();
			playSounds(sound_Bestenliste);
		}
		else
		{
			s1=cE(bestenscreen,"h1","","");
			s1.innerHTML=gStr("best1")+data.aktuellesJahr;
		
			data.gewinnerliste.sort(sortGWL);
		
			ul=cE(bestenscreen,"ul","","");
			for(t=0;t<data.gewinnerliste.length;t++){
				ob=data.gewinnerliste[t];
				li=cE(ul,"li","","");
				s1=cE(li,"span","","");
				s1.innerHTML=ob.d;
				s2=cE(li,"span","","apfel_bs_punke");
				s2.innerHTML=ZahlToStr(ob.p,4);
				s3=cE(li,"span","","");
				s3.innerHTML=clearString(decodeURIComponent(ob.n));
			}
		}
	}
	
	var clearString=function(s){
		s=s.split('<').join('&lt;');
		s=s.split('>').join('&gt;');
		return s;
	}
	
	var cLoader=function(){
		var xmlloader;
		try {// Mozilla, Opera, Safari sowie Internet Explorer (ab v7)
				xmlloader = new XMLHttpRequest();
				if (xmlloader.overrideMimeType) 
					xmlloader.overrideMimeType('text/JSON');
			} 
			catch(e) {
					try {
						// MS Internet Explorer (ab v6)
						xmlloader  = new ActiveXObject("Microsoft.XMLHTTP");
						
					} catch(e) {
						try {
							// MS Internet Explorer (ab v5)
							xmlloader  = new ActiveXObject("Msxml2.XMLHTTP");
						} catch(e) {
							xmlloader  = null;
							console.error('xmlloader nicht möglich...');
						}
					}
		}		
		return xmlloader;
	}
	
	
	var loadBestenliste=function(){		
		var xmlloader=cLoader();				
		if(xmlloader){
				xmlloader.open("GET",data.gewinnerlisteURL+"?d="+(new Date()),true); 
				xmlloader.onreadystatechange = function(){
					if(this.readyState == 4) {
						data.gewinnerliste=JSON.parse(this.responseText).liste;
						if(spielmode==mode_bestenlisteList)
							showBestenliste();
						}
					};
				xmlloader.send(null);
		}		
	}
	
	
	
	var showHelp=function(){
		bestenscreen.innerHTML=gStr("info");		
	}
	
	var sortGWL=function(a,b){
		if(a.p<b.p)return 1;
		else
		if(a.p>b.p)return -1;
		else
		return 0;
	}
	
	var click_save=function(e){
		if(inputbutt)
			inputbutt.style.display="none";
		var s1=cE(bestenscreen,"p","","");
		s1.innerHTML=gStr("stat_speichern");		
		saveUser(inputname.value,data.punkte);
	}
	
	var saveUser=function(name,punkte){
		var dat=new Date();
		var ds=ZahlToStr(dat.getDate(),2)+"."+
			   ZahlToStr(dat.getMonth()+1,2)+"."+
			   ZahlToStr(dat.getFullYear(),4);
		var ob={n:name,p:punkte,d:ds};
		data.gewinnerliste.push(ob);
		
		addBestenliste(ob);
		
		//Show Bestenliste
		setGameMode(mode_bestenlisteList);
	}
	
	var addBestenliste=function(o){// n:name,p:punkte,d:ds 
		var xmlloader=cLoader();
		if(xmlloader){
			// "d":"18.02.2014","n":"Anja",	"p":55 		 
			var fd=new FormData();
			var datum=encodeURIComponent(o.d);
			fd.append("d",datum);
			var punkte=encodeURIComponent(o.p);
			fd.append("p",punkte);
			var name=encodeURIComponent(o.n);
			fd.append("n",name);
			fd.append("c",getcs(datum+punkte+name));
		 
			xmlloader.open("POST",data.gewinnerlisteAddURL,true);		//TODO: auf server speichern
			xmlloader.onreadystatechange = function(){
				if(this.readyState == 4) {
					var status=JSON.parse(this.responseText);
					loadBestenliste();
					}
				};
			xmlloader.send(fd);
		}
	}
	var getcs=function(s){
		var re=0;
		for(var t=0;t<s.length;t++){
			re+=s.charCodeAt(t);
		}
		return re;
	}
		
	var showPunkte=function(){
		punkte.innerHTML=gStr("punkte")+ZahlToStr(data.punkte,4);
	}
	
	var showLeben=function(){
		var t,ob;
		for(t=0;t<leben.length;t++){
			ob=leben[leben.length-t-1];
			if(t<data.leben)
				ob.className="apfel_leben";
				else
				ob.className="apfel_leben apfel_noleben";
		}
	}
	
	var zweiPI100=2*3.141592/100;
	var spielTimer=function(now){//30fps
		var appelDiv=spielball;
		var ifreset=false;
		appelDiv.posY+=(data.ballstep/data.fps*30);
		if(appelDiv.posY>spielscreen.offsetHeight){
			ifreset=true;
		}
		
		if(appelDiv.posY+10>data.positionenYm[data.positionenYm.length-1]){
			appelDiv.style.top=(spielscreen.offsetHeight-appelDiv.offsetHeight)+'px';
			settransform(appelDiv,"rotate(0deg)");
			
			if(appelDiv.className!="apfel_apfelmatsch"){
				appelDiv.className="apfel_apfelmatsch";
				playSounds(sound_Matsch);
			
				if(data.leben>-1)data.leben--;
				showPunkte();
				showLeben();
				}
		}
		else
			{
			if(appelDiv.className!="apfel_apfel")appelDiv.className="apfel_apfel";
			appelDiv.style.top=(appelDiv.posY|0)+'px';//
			if(!is_safari)settransform(appelDiv,"rotate("+(8*Math.sin(zweiPI100*appelDiv.posY))+"deg)");
						
			if(appelDiv.posY+20>data.positionenYm[data.positionenYm.length-1])
				{
					if(
						spielfigur.offsetLeft>appelDiv.offsetLeft-2
						&&
						spielfigur.offsetLeft<appelDiv.offsetLeft+2
					){
						//gefangen
						ifreset=true;
						data.punkte+=(data.ballstep|0);
						data.leben++;
						if(data.leben>leben.length){
							data.leben=0;
							data.ballstep+=0.5;
							}
						showPunkte();
						showLeben();
					}
				}
		}
		
		if(ifreset){	
			if(data.leben<0){
				//Spielende
				if(data.punkte>0)
					setGameMode(mode_bestenlisteIN);
					else
					setGameMode(mode_bestenlisteList);
				data.ballstep=1;
			}		
		
			setspielball();
		}
		
	}
	
	var setGameMode=function(mode){	
		var t,bs;
		spielmode=mode;
		if(spielmode==mode_play){
			bustartpause.innerHTML=gStr("b_pause");
			spielball.style.display="block";
			lebensliste.style.display="block";
			if(data.leben<0){
				data.punkte=0;
				data.leben=0;
			}
			showPunkte();
			showLeben();
			}
			else{
			//mode_pause, mode_bestenlisteList,mode_bestenlisteIN
			bustartpause.innerHTML=gStr("b_start");
			spielball.style.display="none";
			lebensliste.style.display="none";
			}
			
		if(spielmode==mode_bestenlisteList || spielmode==mode_bestenlisteIN|| spielmode==mode_help){
			bestenscreen.style.display="block";
			if(spielmode==mode_help)
				showHelp();
				else
				showBestenliste();
		}
			else
			bestenscreen.style.display="none";
			
		for(t=0;t<data.pausegrafik.length;t++){
			bs=data.pausegrafikdiv[t];
			bs.style.display=(spielmode==mode_pause)?"block":"none";
		}
	}
	
	var switchGameMode=function(){	
		if(spielmode!=mode_play)
			spielmode=mode_play;
			else
			spielmode=mode_pause;						
		setGameMode(spielmode);
	}
	
	var keyDown=function(e){
		//e = e || window.e;
		if(spielmode!=mode_bestenlisteIN){
			if(e.keyCode==72){
					if(spielmode!=mode_help)
						setGameMode(mode_help);
						else
						setGameMode(mode_pause);
					e.cancelBubble = true;
					e.returnValue = false;
			}
		}
		//moveLeft: a:65 <:37   moveRight: >:39 d:68
		if(e.keyCode==65 || e.keyCode==37 || e.keyCode==39 || e.keyCode==68 ){
			if(spielmode==mode_play){
				e.cancelBubble = true;
				e.returnValue = false;
				if(e.keyCode==65 || e.keyCode==37)
					moveFigur(-1);
				if(e.keyCode==39 || e.keyCode==68)
					moveFigur(1);

			}
		}
		if(e.keyCode==32){
			if(spielmode==mode_pause)
				setGameMode(mode_play);
				else
				setGameMode(mode_pause);
			e.cancelBubble = true;
			e.returnValue = false;
		} 	
		return e.returnValue;
	}
	
	var moveFigur=function(r){//-1,(0),+1
		if(r==-1 && data.figur.pos==0){r=0;}
		if(r==1 && data.figur.pos+1>=data.positionenXm.length){r=0;}
		
		if((data.figur.stat==-1 && r==1) || (data.figur.stat==1 && r==-1) ){
			data.figur.stat=0;
		}
		else
		if(data.figur.stat==0 && r==1){
			data.figur.stat=1;
		}
		else
		if(data.figur.stat==0 && r==-1){
			data.figur.stat=-1;
		}
		else
		if( (data.figur.stat==1 && r==1) || (data.figur.stat==-1 && r==-1)){
			data.figur.pos+=r;
		}
		
		switch(data.figur.stat){
			case -1: 
				spielfigur.className="apfel_spielfigurL";
			break;
			case 0: 
				spielfigur.className="apfel_spielfigur";
			break;
			case 1: 
				spielfigur.className="apfel_spielfigurR";
			break;
		
		}
		
		if(data.figur.pos<0)data.figur.pos=0;
		if(data.figur.pos>=data.positionenXm.length)data.figur.pos=data.positionenXm.length-1;
		
		var px=data.positionenXm[data.figur.pos];
		spielfigur.style.left=((px-spielfigur.offsetWidth*0.5)|0)+'px';
		
	}
	
	addEvent(window, 'load', function(){ini() });
}










